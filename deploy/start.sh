#!/bin/bash

set -euo pipefail

keytool -import \
  -alias cluster-ca \
  -file /cluster-ca/ca.crt \
  -keystore "$JAVA_HOME/lib/security/cacerts" \
  -noprompt \
  -storepass changeit

java -jar /gitlab-cache-server.jar
package us.craigmiller160.gitlabcacheserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = ["us.craigmiller160.gitlabcacheserver"])
class GitlabCacheServerApplication

fun main(args: Array<String>) {
  runApplication<GitlabCacheServerApplication>(*args)
}

package us.craigmiller160.gitlabcacheserver.web.controller

import com.fasterxml.jackson.databind.ObjectMapper
import jakarta.servlet.http.HttpServletRequest
import kotlin.io.path.inputStream
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import us.craigmiller160.gitlabcacheserver.web.service.CacheFileService
import us.craigmiller160.gitlabcacheserver.web.types.FileContent
import us.craigmiller160.gitlabcacheserver.web.types.FileList
import us.craigmiller160.gitlabcacheserver.web.types.FileMissing
import us.craigmiller160.gitlabcacheserver.web.types.FileResponse

@RestController
@RequestMapping("/**")
class CacheFileController(
    private val cacheService: CacheFileService,
    private val objectMapper: ObjectMapper
) {
  @GetMapping
  fun getFilesFromCache(request: HttpServletRequest): ResponseEntity<StreamingResponseBody> =
      when (val fileResponse = cacheService.getFromCache(request.requestURI)) {
        is FileList ->
            ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(fileResponse.toJsonStreamingResponseBody())
        is FileContent ->
            ResponseEntity.status(200)
                .contentType(fileResponse.contentType)
                .header(
                    HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=${fileResponse.name}")
                .body(fileResponse.toStreamingResponseBody())
        is FileMissing ->
            ResponseEntity.status(404).body(fileResponse.toJsonStreamingResponseBody())
      }

  private fun FileContent.toStreamingResponseBody(): StreamingResponseBody =
      StreamingResponseBody { outputStream ->
        this.path.inputStream().use { inputStream -> inputStream.transferTo(outputStream) }
      }

  private fun FileResponse.toJsonStreamingResponseBody(): StreamingResponseBody =
      StreamingResponseBody { outputStream ->
        objectMapper.writeValue(outputStream, this)
      }
}

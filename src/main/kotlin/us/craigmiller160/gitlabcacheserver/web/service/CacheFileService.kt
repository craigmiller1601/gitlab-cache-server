package us.craigmiller160.gitlabcacheserver.web.service

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.ZoneId
import kotlin.io.path.extension
import kotlin.streams.asSequence
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import us.craigmiller160.gitlabcacheserver.config.GitlabCacheConfig
import us.craigmiller160.gitlabcacheserver.web.types.FileContent
import us.craigmiller160.gitlabcacheserver.web.types.FileList
import us.craigmiller160.gitlabcacheserver.web.types.FileListItem
import us.craigmiller160.gitlabcacheserver.web.types.FileMissing
import us.craigmiller160.gitlabcacheserver.web.types.FileResponse

@Service
class CacheFileService(private val cacheConfig: GitlabCacheConfig) {
  companion object {
    private const val FILE = "file"
    private const val DIRECTORY = "directory"
  }

  private val log = LoggerFactory.getLogger(javaClass)

  fun getFromCache(requestPath: String): FileResponse {
    log.info("Getting file(s) from cache for: $requestPath")
    val targetPath = Paths.get(cacheConfig.root, *requestPath.split("/").toTypedArray())
    if (!Files.exists(targetPath)) {
      return targetPath.toFileMissing()
    }

    if (Files.isDirectory(targetPath)) {
      return targetPath.toFileList()
    }

    return targetPath.toFileContent()
  }

  private fun Path.toFileMissing(): FileMissing = FileMissing(this)

  private fun Path.toFileContent(): FileContent {
    val contentType: MediaType =
        when (this.extension) {
          "txt" -> MediaType.TEXT_PLAIN
          "zip" -> MediaType("application", "zip")
          else -> throw IllegalArgumentException("Unsupported file extension: $this")
        }

    return FileContent(path = this, contentType = contentType)
  }

  private fun Path.toFileList(): FileList =
      Files.list(this)
          .asSequence()
          .map { file ->
            val lastModifiedTime =
                Files.getLastModifiedTime(file).toInstant().atZone(ZoneId.of("UTC"))
            FileListItem(path = file, lastModified = lastModifiedTime)
          }
          .groupBy { item -> if (Files.isDirectory(item.path)) DIRECTORY else FILE }
          .let { groups ->
            FileList(directories = groups[DIRECTORY] ?: listOf(), files = groups[FILE] ?: listOf())
          }
          .let { fileList ->
            fileList.copy(
                directories = fileList.directories.sortedByDescending { it.lastModified },
                files = fileList.files.sortedByDescending { it.lastModified })
          }

  private fun Path.formatFile(): String {
    if (Files.isDirectory(this)) {
      return "${this}/"
    }
    return this.toString()
  }
}

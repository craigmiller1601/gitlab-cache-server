package us.craigmiller160.gitlabcacheserver.web.types

import java.nio.file.Path
import java.time.ZonedDateTime
import org.springframework.http.MediaType

sealed interface FileResponse

data class FileListItem(val path: Path, val lastModified: ZonedDateTime)

data class FileList(val directories: List<FileListItem>, val files: List<FileListItem>) :
    FileResponse

data class FileContent(val path: Path, val contentType: MediaType) : FileResponse {
  val name: String = path.fileName.toString()
}

data class FileMissing(val path: Path) : FileResponse

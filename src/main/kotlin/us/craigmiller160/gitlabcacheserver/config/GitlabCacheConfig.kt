package us.craigmiller160.gitlabcacheserver.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "gitlab.cache") data class GitlabCacheConfig(val root: String)
